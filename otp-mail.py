#!/usr/bin/env python3
"""Mail utility like mailx

Usage:
    otp-mail [-dv] [-s subject] [-c cc-addr...] [-b bcc-addr...] <to-addr>...

Options:
    -v          Verbose mode
    -d          Debugging mod
    -s subject  Specify subject on command line [default: ]
    -c cc-addr  Send carbon copy to cc-addr
    -b bcc-addr Send carbon copy to cc-addr

content read from stdin
"""

import os
import subprocess
import sys

from docopt import docopt

from oauth2_smtp.config import load_config
from oauth2_smtp.smtp_client import SmtpClient


if __name__ == "__main__":
    cwd = os.path.dirname(os.path.realpath(__file__))
    cmd = ["git", "describe", "--always", "--tags", "--dirty"]
    if sys.version_info < (2, 7, 0):
        version = subprocess.Popen(cmd, stdout=subprocess.PIPE, cwd=cwd).communicate()[0]
    else:
        version = subprocess.check_output(cmd, cwd=cwd)
    args = docopt(__doc__, version=version)
    # print(args)

    content = ''
    for line in sys.stdin:
        content += line

    conf = load_config()

    to_addresses = args['<to-addr>']
    cc_addresses = args['-c']
    bcc_addresses = args['-b']
    subject = args['-s']
    verbose = args['-v']
    debug = args['-d']

    z = SmtpClient(conf, verbose, debug)
    z.process_smtp(to_addresses, subject, content, cc_addresses, bcc_addresses)

"""Process incoming emails."""
import base64
from oauth2_smtp.config import Config
from oauth2_smtp.oauth2_flow import Oauth2Flow
import smtplib


class SmtpClient:
    """Process outgoing emails."""

    def __init__(self, conf: Config, verbose, debug) -> None:
        """Initialize the Imap client."""
        self.conf = conf
        self.verbose = verbose
        self.debug = debug
        self.oauth = Oauth2Flow(conf, verbose, debug)

    def process_smtp(self, to_addresses, subject, content, cc_addresses, bcc_addresses) -> None:
        """Process normal imaplib authentication"""
        # Authenticate to account using OAuth 2.0 mechanism
        access_token, username = self.oauth.get_access_token()
        # Here smtp wants a base64 token, encode set to True
        auth_string = self.sasl_xoauth2(username, access_token, True)
        if self.verbose:
            print("username", username)
        smtp_conn = smtplib.SMTP(self.conf.SMTP_SERVER, 587, local_hostname="cern.ch")
        smtp_conn.set_debuglevel(self.debug)
        smtp_conn.starttls()
        # we need to send another hello after starttls
        smtp_conn.ehlo("cern.ch")
        smtp_conn.docmd('AUTH', 'XOAUTH2 ' + auth_string)
        from_address = username
        all_to_addresses = to_addresses + cc_addresses + bcc_addresses
        smtp_conn.sendmail(
            from_address,
            all_to_addresses,
            "From: " + from_address + "\ncc: " + (", ".join(cc_addresses)) + "\nbcc: " + (", ".join(bcc_addresses)) + "\nTo: " + (", ".join(to_addresses)) + "\nSubject: " + subject + "\n\n" + content
        )
        smtp_conn.quit()

    def sasl_xoauth2(self, username, access_token, base64_encode=False) -> str:
        """Convert the access_token into XOAUTH2 format"""
        auth_string = "user=%s\1auth=Bearer %s\1\1" % (username, access_token)
        if base64_encode:
            auth_string = base64.b64encode(auth_string.encode("ascii")).decode("ascii")
        return auth_string
